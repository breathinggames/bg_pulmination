﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    private Camera cam;
    public float topLimit;
    public float bottomLimit;
    private Vector3 originalPosition;
    [HideInInspector]
    public bool goBack = false;
    public float goBackSpeed = 10;

    private void Start()
    {
        cam = Camera.main;
        originalPosition = cam.transform.position;
    }

	void Update () {
        if (goBack)
        {
            cam.transform.Translate(Vector3.down * Time.deltaTime * goBackSpeed);
            if (Vector3.Distance(cam.transform.position,originalPosition) < 0.1f)
            {
                goBack = false;
            }
            return;
        }
        Vector3 viewportPos = cam.ScreenToViewportPoint(Input.mousePosition);
        if (viewportPos.y > 0.95 || viewportPos.y < 0.05)
        {
            float offset = 0.0f;
            if (viewportPos.y > 0.95)
            {
                offset = viewportPos.y - 0.95f;
            }
            if (viewportPos.y < 0.05)
            {
                offset = viewportPos.y - 0.05f;
            }
            Vector3 newCamPos = cam.transform.position = cam.transform.position + new Vector3(0, offset);
            if (newCamPos.y > topLimit)
            {
                newCamPos.y = topLimit;
                cam.transform.position = newCamPos;
            }
            else if (newCamPos.y < bottomLimit)
            {
                newCamPos.y = bottomLimit;
                cam.transform.position = newCamPos;
            }
            else
            {
                cam.transform.position = newCamPos;
            }

        }
    }
}
