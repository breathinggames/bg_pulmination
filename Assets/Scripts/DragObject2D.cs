﻿using UnityEngine;
using System.Collections;

public class DragObject2D : MonoBehaviour {

    private Camera cam;
    private Transform t;
    public float speed;
    private Rigidbody2D r;
    private Collider2D c;
    private int layerMask = 8;

	void Start () {
        cam = Camera.main;
    }

    void Update() {


        if (Input.GetMouseButton(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(cam.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, Mathf.Infinity, 1 << LayerMask.NameToLayer("Draggable"));

            if (Input.GetMouseButtonDown(0) && hit.collider != null)
            {
                r = hit.rigidbody;
                t = hit.transform;
                c = hit.collider;
                c.gameObject.GetComponent<Enemy>().isBeingDragged = true;
            }

            if (Input.GetMouseButton(0) && r != null && t != null && c != null)
            {
                Vector3 pos = Input.mousePosition;
                pos = cam.ScreenToWorldPoint(pos);
                r.velocity = (pos - t.position) * speed;
                r.angularVelocity = 0;
            }           
        }
        if (Input.GetMouseButtonUp(0) && r != null && t != null && c != null)
        {
            r = null;
            t = null;
            c.gameObject.GetComponent<Enemy>().isBeingDragged = false;
            c = null;
        }

    }
}
