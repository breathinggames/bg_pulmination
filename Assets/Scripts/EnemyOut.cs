﻿using UnityEngine;
using System.Collections;

public class EnemyOut : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            Destroy(collision.gameObject);
            GameController gc = GameObject.Find("GameController").GetComponent<GameController>();
            gc.numberEnemiesLeft--;

            
        }
    }
}
