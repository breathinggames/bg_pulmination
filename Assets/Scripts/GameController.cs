﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GameController : MonoBehaviour {

    public List<GameObject> typeEnemies;
    private List<GameObject> currentEnemies;
    public Enemy enemy;
    public int initNumberEnemies;
    private int typeCurrentEnemy = 0;
    EdgeCollider2D[] edgeColliders;
    [HideInInspector]
    public int numberEnemiesLeft;
    public float[] SpawnAreaLimits;
    private Camera cam;
    public GameObject roundText;

    void Start () {
        cam = Camera.main;
        edgeColliders = GameObject.FindGameObjectWithTag("Lungs").GetComponents<EdgeCollider2D>();
        AddEnemies(typeCurrentEnemy);
        DisplayNextRoundText();

    }

    private void AddEnemies(int typeCurrentEnemy)
    {
        currentEnemies = new List<GameObject>();
        for (int j = 0; j < initNumberEnemies; j++)
        {              
            float minX = SpawnAreaLimits[0];
            float maxX = SpawnAreaLimits[1];
            float minY = SpawnAreaLimits[2];
            float maxY = SpawnAreaLimits[3];
            Vector3 position = new Vector3(UnityEngine.Random.Range(minX, maxX), UnityEngine.Random.Range(minY, maxY), -1);
            GameObject enemyInstantiated = (GameObject)Instantiate(typeEnemies[typeCurrentEnemy], position, Quaternion.identity);
            currentEnemies.Add(enemyInstantiated);
        }
        
        numberEnemiesLeft = currentEnemies.Count;
    }

    private List<float> getBoundaries(Vector2[] pointsCollider)
    {
        List<float> minmax = new List<float>();
        float minX = Mathf.Infinity;
        float maxX = -Mathf.Infinity;
        float minY = Mathf.Infinity;
        float maxY = -Mathf.Infinity;

        for (int i = 0; i < pointsCollider.Length; i++)
        {
            if (minX > pointsCollider[i].x)
            {
                minX = pointsCollider[i].x;
            }
            if (maxX < pointsCollider[i].x)
            {
                maxX = pointsCollider[i].x;
            }
            if (minY > pointsCollider[i].y)
            {
                minY = pointsCollider[i].y;
            }
            if (maxY < pointsCollider[i].y)
            {
                maxY = pointsCollider[i].y;
            }
        }
        minmax.Add(minX);
        minmax.Add(maxX);
        minmax.Add(minY);
        minmax.Add(maxY);

        return minmax;
    }

    void Update () {
        Debug.Log(numberEnemiesLeft);
        if (numberEnemiesLeft == 0)
        {
            if (typeCurrentEnemy == typeEnemies.Count)
            {
                // Game Over
                Debug.Log("Game Over");
            }
            else
            {
                cam.GetComponent<CameraFollow>().goBack = true;
                typeCurrentEnemy++;
                DisplayNextRoundText();
                AddEnemies(typeCurrentEnemy);
            }
        }
    }

    private void DisplayNextRoundText()
    {
        roundText.GetComponent<UnityEngine.UI.Text>().text = "Round " + (typeCurrentEnemy + 1);
        roundText.GetComponent<Animator>().SetTrigger("NextRound");
    }
}
