﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour {

    public int sceneToStart = 1;

	void Start () {

	}
	
	void Update () {
	
	}

    public void StartButtonClicked()
    {
        SceneManager.LoadScene(sceneToStart);
    }
}
