﻿using UnityEngine;
using System.Collections;

public class Dust : Enemy
{
    public float minimum = 0.0f;
    public float maximum = 1f;
    public float duration = 5.0f;
    private float startTime;
    private SpriteRenderer sprite;
    private bool fadeIn = false;
    private bool fadeOut = true;

    void Start()
    {
        sprite = gameObject.GetComponent<SpriteRenderer>();
        startTime = Time.time;
    }
    override
    public void move()
    {
        gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-speed, speed), Random.Range(-speed, speed)));
    }

    void Update()
    {
        if (sprite.color.a == 1f && fadeIn)
        {
            startTime = Time.time;
            fadeIn = false;
            fadeOut = true;
        }
        if (sprite.color.a == 0.0f && fadeOut)
        {
            startTime = Time.time;
            fadeIn = true;
            fadeOut = false;
        }
        if (fadeIn)
        {
            float t = (Time.time - startTime) / duration;
            sprite.color = new Color(1f, 1f, 1f, Mathf.SmoothStep(minimum, maximum, t));
        }
        else if (fadeOut)
        {
            float t = (Time.time - startTime) / duration;
            sprite.color = new Color(1f, 1f, 1f, Mathf.SmoothStep(maximum, minimum, t));
        }
    }
}
