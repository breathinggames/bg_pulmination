﻿using UnityEngine;
using System.Collections;

public abstract class Enemy : MonoBehaviour
{

    public float speed = 15.0f;
    [HideInInspector]
    public bool isBeingDragged = false;

    void Start()
    {

    }

    void Update()
    {
        if (isBeingDragged)
        {
            gameObject.GetComponent<SpriteRenderer>().color = new Color(241, 255, 0); // Yellow tint
        } else if (!isBeingDragged)
        {
            gameObject.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255); // White
        }
    }

    void FixedUpdate()
    {
        move();
    }

    public abstract void move();
}
