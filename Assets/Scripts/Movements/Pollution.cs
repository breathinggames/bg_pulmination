﻿using UnityEngine;
using System.Collections;

public class Pollution : Enemy
{

    override
    public void move()
    {
        gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-speed, speed), 0));
    }
}
