﻿using UnityEngine;
using System.Collections;

public class Smoke : Enemy {

    override
    public void move()
    {
        gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 1));
    }
}
