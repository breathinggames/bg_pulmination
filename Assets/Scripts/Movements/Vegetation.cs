﻿using UnityEngine;
using System.Collections;

public class Vegetation : Enemy
{

    public GameObject chasee;
    public float randomness = 1;
    private GameObject[] boids;
    private int flockSize;
    private Vector2 flockCenter;
    private Vector2 flockVelocity;

    void Awake()
    {
        Physics.IgnoreLayerCollision(8, 8, true);
    }
    void Start()
    {
        boids = GameObject.FindGameObjectsWithTag("Vegetation");
        flockSize = boids.Length;
    }
    override
    public void move()
    {
        if (chasee)
        {
            Vector2 randomize = new Vector2((Random.value * 2) - 1, (Random.value * 2) - 1);
            randomize.Normalize();
            Vector2 chaseePosition = new Vector2(chasee.transform.position.x, chasee.transform.position.y);
            Vector2 follow = chaseePosition;
            Vector2 position = new Vector2(transform.position.x, transform.position.y);

            flockCenter = flockCenter - position;
            flockVelocity = flockVelocity - GetComponent<Rigidbody2D>().velocity;
            follow = follow - position;

            GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity + (flockCenter + flockVelocity + follow * 2 + randomize * randomness) * Time.deltaTime;
        }
        else
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-speed, speed), Random.Range(-speed, speed)));
        }
    }

    void Update()
    {
        Vector2 theCenter = Vector2.zero;
        Vector2 theVelocity = Vector2.zero;

        foreach (GameObject boid in boids)
        {
            Vector2 boidPosition = new Vector2(boid.transform.position.x, boid.transform.position.y);
            Vector2 boidVelocity = boid.GetComponent<Rigidbody2D>().velocity;
            theCenter = theCenter + boidPosition;
            theVelocity = theVelocity + boidVelocity;
        }
        flockCenter = theCenter / (flockSize);
        flockVelocity = theVelocity / (flockSize);
    }
}
